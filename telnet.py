import telnetlib
import time
import turtle


def main():
    while True:
        meum()

        try:
            choice = int(input('请选择:'))
        except:
            print('请选择正确的选项')
        else:
            if choice in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
                if choice == 0:
                    answer = input('您确定好退出系统吗？y/n')
                    if answer == 'y' or answer == 'Y':
                        print('谢谢您的使用！！！')
                        break
                    else:
                        continue
                elif choice == 1:
                    hfvlan()
                elif choice == 2:
                    pzvlandz()
                elif choice == 3:
                    pzdk()
                elif choice == 4:
                    jtly()
                elif choice == 5:
                    pass
                elif choice == 6:
                    cx()
                elif choice == 7:
                    qcdkpz()
                elif choice == 8:
                    bdd()
                elif choice == 9:
                    bcpz()


def hfvlan():
    while True:
        ml = input("请输入vlan号：")
        telnet_login.write(b'vlan batch ' + ml.encode('ascii') + b"\n")
        print("命令：vlan batch " + ml)
        time.sleep(3)
        print("你划分的vlan号：" + ml)
        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def pzvlandz():
    while True:
        ml = input("请输入配置地址的vlan号：")
        telnet_login.write(b'interface Vlanif ' + ml.encode('ascii') + b"\n")
        time.sleep(3)
        print("注意！！！\nIP地址和子网掩码中间必须有一个空格")
        network = input("请输入IP地址和子网掩码：")
        telnet_login.write(b'ip address ' + network.encode('ascii') + b"\n")
        time.sleep(3)
        print("命令：interface Vlanif " + ml)
        print("命令：ip address " + network)
        telnet_login.write(b'quit' + b"\n")
        time.sleep(3)
        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def pzdk():
    while True:
        ml = input("请输入配置的端口号：")
        telnet_login.write(b'interface GigabitEthernet 0/0/' + ml.encode('ascii') + b"\n")
        print("命令：interface GigabitEthernet 0/0/" + ml)
        time.sleep(3)
        print("注意！！！\naccess 连接一个终端，只能属于一个vlan\ntrunk 透传连接交换机，可以通过多个vlan")

        print("\t\t\t\t1.access")
        print("\t\t\t\t2.trunk")
        dkms = int(input("请选择端口模式："))
        if dkms == 1:
            telnet_login.write(b'port link-type access' + b"\n")
            print("命令：port link-type access")
            time.sleep(3)
            print("将端口加入vlan！！！")
            vh = input("请输入端口所属的vlan号:")
            telnet_login.write(b'port default vlan ' + vh.encode('ascii') + b"\n")
            print("命令：port default vlan " + vh)
            time.sleep(3)
            telnet_login.write(b'quit' + b"\n")
            time.sleep(3)
            answer = input("是否返回上一级？y/n")
            if answer == 'y' or answer == 'Y':

                break
            else:
                continue
        elif dkms == 2:
            telnet_login.write(b'port link-type trunk' + b"\n")
            print("命令：port link-type trunk")
            time.sleep(3)
            telnet_login.write(b'port trunk allow-pass vlan all' + b"\n")
            print("命令：port trunk allow-pass vlan all")
            time.sleep(3)
            telnet_login.write(b'quit' + b"\n")
            time.sleep(3)
            answer = input("是否返回上一级？y/n")
            if answer == 'y' or answer == 'Y':

                break
            else:
                continue

        elif dkms != 1 or dkms != 2:
            answer = input("是否返回上一级？y/n")
            if answer == 'y' or answer == 'Y':

                break
            else:
                continue


def qcdkpz():
    while True:
        ml = input("请输入需要清除信息的端口号：")
        telnet_login.write(b'clear configuration interface GigabitEthernet 0/0/' + ml.encode('ascii') + b"\n")
        print("命令：clear configuration interface GigabitEthernet 0/0/" + ml)
        time.sleep(3)
        telnet_login.write(b'y' + b"\n")
        time.sleep(3)
        telnet_login.write(b'interface GigabitEthernet 0/0/' + ml.encode('ascii') + b"\n")
        time.sleep(3)
        telnet_login.write(b'undo shutdown' + b"\n")
        time.sleep(3)
        telnet_login.write(b'quit' + b"\n")
        time.sleep(3)
        print("端口信息清除完成！！！")

        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def bcpz():
    while True:
        telnet_login.write(b'return' + b"\n")
        time.sleep(3)
        telnet_login.write(b'save' + b"\n")
        print("命令：save")
        time.sleep(3)
        telnet_login.write(b'y' + b"\n")
        time.sleep(3)
        telnet_login.write(b"\n\n\n")
        time.sleep(3)
        telnet_login.write(b"sys" + b"\n")
        time.sleep(3)
        print("配置保存成功！！！")

        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def jtly():
    while True:
        print("注意：下一跳的地址不能为本设备的所有地址")
        print("格式：x.x.x.x x.x.x.x x.x.x.x")
        print("例子：0.0.0.0 0.0.0.0 192.168.1.1")
        y = input("请输入源地址 源子网掩码 下一跳地址：")
        telnet_login.write(b'ip route-static ' + y.encode('ascii') + b"\n")
        print("命令：ip route-static " + y)
        xx = telnet_login.read_very_eager().decode('ascii')
        time.sleep(3)
        print(xx)
        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def cx():
    print("查询准备中，请稍后……")
    telnet_login.write(b'user-interface vty 0 4' + b"\n")
    time.sleep(2)
    # telnet_login.write(b'' + b"\n")
    telnet_login.write(b'screen-length 0' + b"\n")
    time.sleep(2)
    telnet_login.write(b'quit' + b"\n")
    time.sleep(2)

    while True:
        print("\t\t\t\t1.查询当前配置")
        print("\t\t\t\t2.查询Vlan和接口")
        print("\t\t\t\t3.查询Vlan接口与IP相关信息")
        print("\t\t\t\t4.查询所有接口的相关配置")
        print("\t\t\t\t0.返回上一级")
        try:
            dkms = int(input("请选择查询的内容："))
        except:
            print('请选择正确的选项')
        else:
            if dkms == 1:
                telnet_login.write(b'display current-configuration' + b"\n")
                print("命令：display current-configuration")
                print("查询中...")
                time.sleep(3)
                xx = telnet_login.read_very_eager().decode('ascii')
                time.sleep(3)
                print(xx)

                answer = input("是否返回上一级？y/n")
                if answer == 'y' or answer == 'Y':

                    break
                else:
                    continue
            elif dkms == 2:
                telnet_login.write(b'display vlan' + b"\n")
                print("命令：display vlan")
                print("查询中...")
                time.sleep(3)
                xx = telnet_login.read_very_eager().decode('ascii')
                time.sleep(3)
                print(xx)

                answer = input("是否返回上一级？y/n")
                if answer == 'y' or answer == 'Y':

                    break
                else:
                    continue
            elif dkms == 3:
                telnet_login.write(b'display ip interface brief' + b"\n")
                print("命令：display ip interface brief")
                print("查询中...")
                time.sleep(3)
                xx = telnet_login.read_very_eager().decode('ascii')
                time.sleep(3)
                print(xx)

                answer = input("是否返回上一级？y/n")
                if answer == 'y' or answer == 'Y':
                    break
                else:
                    continue
            elif dkms == 4:
                telnet_login.write(b'display current-configuration interface' + b"\n")
                print("命令：display current-configuration interface")
                print("查询中...")
                time.sleep(3)
                xx = telnet_login.read_very_eager().decode('ascii')
                time.sleep(3)
                print(xx)

                answer = input("是否返回上一级？y/n")
                if answer == 'y' or answer == 'Y':
                    break
                else:
                    continue

            elif dkms == 0:
                answer = input("是否返回上一级？y/n")
                if answer == 'y' or answer == 'Y':

                    break
                else:
                    continue


def sm():
    print("此程序是由telnet连接交换机\n如果交换机没有配置telnet，请继续往下看")
    print("下面的一段命令 主要功能是 新建一个用户名为test 密码为Test@123")
    print("用户test开启telnet登录 新建Vlan100 IP：192.168.100.1 并把端口1加入了vlan 100")
    print("如果华为交换机没有开启telnet，可以通过console来开启telnet，只需要直接复制下面的命令粘贴")
    print("--------------------------------------------------------")
    print("""undo terminal monitor
system-view
aaa
local-user test password cipher Test@123 privilege level 15
local-user test service-type telnet
quit
user-interface vty 0 4
authentication-mode aaa
user privilege level 15
quit
vlan 100
quit
interface Vlanif 100
ip address 192.168.100.1 24
quit
interface GigabitEthernet 0/0/1
port link-type access
port default vlan 100
quit
telnet server enable""")
    print("--------------------------------------------------------")
    print("复制以上命令通过console口来开启telnet")


def bdd():
    turtle.title('Python（冰墩墩）')  # 可以自己修改名称
    turtle.speed(40)  # 可以自己调节速度
    # 左手
    turtle.penup()

    turtle.goto(177, 112)

    turtle.pencolor("lightgray")

    turtle.pensize(3)

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(80)

    turtle.circle(-45, 200)

    turtle.circle(-300, 23)

    turtle.end_fill()

    # 左手内
    turtle.penup()

    turtle.goto(182, 95)

    turtle.pencolor("black")

    turtle.pensize(1)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.setheading(95)

    turtle.pendown()

    turtle.circle(-37, 160)

    turtle.circle(-20, 50)

    turtle.circle(-200, 30)

    turtle.end_fill()

    # 轮廓
    # 头顶
    turtle.penup()

    turtle.goto(-73, 230)

    turtle.pencolor("lightgray")

    turtle.pensize(3)

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(20)

    turtle.circle(-250, 35)

    # 左耳
    turtle.setheading(50)

    turtle.circle(-42, 180)

    # 左侧
    turtle.setheading(-50)

    turtle.circle(-190, 30)

    turtle.circle(-320, 45)

    # 左腿
    turtle.circle(120, 30)

    turtle.circle(200, 12)

    turtle.circle(-18, 85)

    turtle.circle(-180, 23)

    turtle.circle(-20, 110)

    turtle.circle(15, 115)

    turtle.circle(100, 12)

    # 右腿
    turtle.circle(15, 120)

    turtle.circle(-15, 110)

    turtle.circle(-150, 30)

    turtle.circle(-15, 70)

    turtle.circle(-150, 10)

    turtle.circle(200, 35)

    turtle.circle(-150, 20)

    # 右手
    turtle.setheading(-120)

    turtle.circle(50, 30)

    turtle.circle(-35, 200)

    turtle.circle(-300, 23)

    # 右侧
    turtle.setheading(86)

    turtle.circle(-300, 26)

    # 右耳
    turtle.setheading(122)

    turtle.circle(-53, 160)

    turtle.end_fill()

    # 右耳内
    turtle.penup()

    turtle.goto(-130, 180)

    turtle.pencolor("black")

    turtle.pensize(1)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(120)

    turtle.circle(-28, 160)

    turtle.setheading(210)

    turtle.circle(150, 20)

    turtle.end_fill()

    # 左耳内
    turtle.penup()

    turtle.goto(90, 230)

    turtle.setheading(40)

    turtle.begin_fill()

    turtle.pendown()

    turtle.circle(-30, 170)

    turtle.setheading(125)

    turtle.circle(150, 23)

    turtle.end_fill()

    # 右手内
    turtle.penup()

    turtle.goto(-180, -55)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.setheading(-120)

    turtle.pendown()

    turtle.circle(50, 30)

    turtle.circle(-27, 200)

    turtle.circle(-300, 20)

    turtle.setheading(-90)

    turtle.circle(300, 14)

    turtle.end_fill()

    # 左腿内
    turtle.penup()

    turtle.goto(108, -168)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(-115)

    turtle.circle(110, 15)

    turtle.circle(200, 10)

    turtle.circle(-18, 80)

    turtle.circle(-180, 13)

    turtle.circle(-20, 90)

    turtle.circle(15, 60)

    turtle.setheading(42)

    turtle.circle(-200, 29)

    turtle.end_fill()

    # 右腿内
    turtle.penup()

    turtle.goto(-38, -210)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(-155)

    turtle.circle(15, 100)

    turtle.circle(-10, 110)

    turtle.circle(-100, 30)

    turtle.circle(-15, 65)

    turtle.circle(-100, 10)

    turtle.circle(200, 15)

    turtle.setheading(-14)

    turtle.circle(-200, 27)

    turtle.end_fill()

    # 右眼
    # 眼圈
    turtle.penup()

    turtle.goto(-64, 120)

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(40)

    turtle.circle(-35, 152)

    turtle.circle(-100, 50)

    turtle.circle(-35, 130)

    turtle.circle(-100, 50)

    turtle.end_fill()

    # 眼珠
    turtle.penup()

    turtle.goto(-47, 55)

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(25, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(-45, 62)

    turtle.pencolor("darkslategray")

    turtle.fillcolor("darkslategray")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(19, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(-45, 68)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(10, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(-47, 86)

    turtle.pencolor("white")

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(5, 360)

    turtle.end_fill()

    # 左眼
    # 眼圈
    turtle.penup()

    turtle.goto(51, 82)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(120)

    turtle.circle(-32, 152)

    turtle.circle(-100, 55)

    turtle.circle(-25, 120)

    turtle.circle(-120, 45)

    turtle.end_fill()

    # 眼珠
    turtle.penup()

    turtle.goto(79, 60)

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(24, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(79, 64)

    turtle.pencolor("darkslategray")

    turtle.fillcolor("darkslategray")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(19, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(79, 70)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(10, 360)

    turtle.end_fill()

    turtle.penup()

    turtle.goto(79, 88)

    turtle.pencolor("white")

    turtle.fillcolor("white")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(0)

    turtle.circle(5, 360)

    turtle.end_fill()

    # 鼻子
    turtle.penup()

    turtle.goto(37, 80)

    turtle.fillcolor("black")

    turtle.begin_fill()

    turtle.pendown()

    turtle.circle(-8, 130)

    turtle.circle(-22, 100)

    turtle.circle(-8, 130)

    turtle.end_fill()

    # 嘴
    turtle.penup()

    turtle.goto(-15, 48)

    turtle.setheading(-36)

    turtle.begin_fill()

    turtle.pendown()

    turtle.circle(60, 70)

    turtle.setheading(-132)

    turtle.circle(-45, 100)

    turtle.end_fill()

    # 彩虹圈
    turtle.penup()

    turtle.goto(-135, 120)

    turtle.pensize(5)

    turtle.pencolor("cyan")

    turtle.pendown()

    turtle.setheading(60)

    turtle.circle(-165, 150)

    turtle.circle(-130, 78)

    turtle.circle(-250, 30)

    turtle.circle(-138, 105)

    turtle.penup()

    turtle.goto(-131, 116)

    turtle.pencolor("slateblue")

    turtle.pendown()

    turtle.setheading(60)

    turtle.circle(-160, 144)

    turtle.circle(-120, 78)

    turtle.circle(-242, 30)

    turtle.circle(-135, 105)

    turtle.penup()

    turtle.goto(-127, 112)

    turtle.pencolor("orangered")

    turtle.pendown()

    turtle.setheading(60)

    turtle.circle(-155, 136)

    turtle.circle(-116, 86)

    turtle.circle(-220, 30)

    turtle.circle(-134, 103)

    turtle.penup()

    turtle.goto(-123, 108)

    turtle.pencolor("gold")

    turtle.pendown()

    turtle.setheading(60)

    turtle.circle(-150, 136)

    turtle.circle(-104, 86)

    turtle.circle(-220, 30)

    turtle.circle(-126, 102)

    turtle.penup()

    turtle.goto(-120, 104)

    turtle.pencolor("greenyellow")

    turtle.pendown()

    turtle.setheading(60)

    turtle.circle(-145, 136)

    turtle.circle(-90, 83)

    turtle.circle(-220, 30)

    turtle.circle(-120, 100)

    turtle.penup()

    # 爱心
    turtle.penup()

    turtle.goto(220, 115)

    turtle.pencolor("brown")

    turtle.pensize(1)

    turtle.fillcolor("brown")

    turtle.begin_fill()

    turtle.pendown()

    turtle.setheading(36)

    turtle.circle(-8, 180)

    turtle.circle(-60, 24)

    turtle.setheading(110)

    turtle.circle(-60, 24)

    turtle.circle(-8, 180)

    turtle.end_fill()

    # 五环
    turtle.penup()

    turtle.goto(-5, -170)

    turtle.pendown()

    turtle.pencolor("blue")

    turtle.circle(6)

    turtle.penup()

    turtle.goto(10, -170)

    turtle.pendown()

    turtle.pencolor("black")

    turtle.circle(6)

    turtle.penup()

    turtle.goto(25, -170)

    turtle.pendown()

    turtle.pencolor("brown")

    turtle.circle(6)

    turtle.penup()

    turtle.goto(2, -175)

    turtle.pendown()

    turtle.pencolor("lightgoldenrod")

    turtle.circle(6)

    turtle.penup()

    turtle.goto(16, -175)

    turtle.pendown()

    turtle.pencolor("green")

    turtle.circle(6)

    turtle.penup()

    turtle.pencolor("black")

    turtle.goto(-16, -160)

    turtle.write("BEIJING 2022", font=('Arial', 10, 'bold italic'))

    turtle.hideturtle()

    # 自定义内容，会显示在图片上方一行文字
    turtle.pencolor("black")

    turtle.goto(-96, 260)

    turtle.write("送给所有苦逼的打工人！！！----HX", font=('新宋体', 20, 'bold italic'))  # 调整文案，字体，字号

    turtle.hideturtle()

    turtle.done()


def meum():
    print("---------------------------------------功能菜单---------------------------------------")
    print("\t\t\t\t1.划分VLAN")
    print('')
    print("\t\t\t\t2.配置VLAN地址")
    print('')
    print("\t\t\t\t3.配置端口信息")
    print('')
    print("\t\t\t\t4.配置静态路由")
    print('')
    print("\t\t\t\t5.不知道添加点什么了")
    print('')
    print("\t\t\t\t6.查询")
    print('')
    print("\t\t\t\t7.清除端口配置信息")
    print('')
    print("\t\t\t\t8.送给您的冰墩墩")
    print('')
    print("\t\t\t\t9.保存交换机配置")
    print('')
    print("\t\t\t\t0.结束程序")
    print("-------------------------------------------------------------------------------------")


if __name__ == '__main__':

    print("程序加载中…… ")
    print("程序加载完成，请您开心愉快的使用！！！ ")
    sm()
    time.sleep(1)
    print("注意 注意 注意 ！ ！ ！ \n使用本程序前，必须保证用户名和密码的正确性。\n否则后面的任何操作将是无效操作！！！")

    host = input("请输入交换机telnet连接地址：")
    print("连接中，请稍等！！！")
    a = 0
    while True:
        try:

            telnet_login = telnetlib.Telnet(host)
        # telnet_login.read_until(b"Username:")
        # telnet_login.write(b"ls\n")
        except TimeoutError:
            print("由于连接方在一段时间后没有正确答复或连接的主机没有反应，连接尝试失败。")
            host = input("请输入交换机telnet连接地址：")
            print("连接中，请稍等！！！")
            continue
        else:

            time.sleep(3)
            xx = telnet_login.read_very_eager().decode('ascii')
            print(xx)

            user = input("请输入用户名：")
            print("提交中，请稍后")
            telnet_login.read_until(b'Username:', timeout=3)
            telnet_login.write(user.encode('ascii') + b"\n")
            time.sleep(3)
            # xx = telnet_login.read_very_eager().decode('ascii')
            # print(xx)
            pw = input("请输入密码：")
            print("提交中，请稍后")
            telnet_login.read_until(b'Password:', timeout=3)
            telnet_login.write(pw.encode('ascii') + b"\n")
            index, obj, output = telnet_login.expect([b'>'], timeout=3)
            if index == -1:
                print('用户名或者密码错误，程序重启！')
                a = a + 1
                if a == 3:
                    print("你都已经输错3次了！！！")
                    continue
                elif a == 9:
                    print('你都整错了9次了，求放过吧！！！')
                    break
                else:
                    continue
            else:
                print('认证成功')
            time.sleep(3)
            xx = telnet_login.read_very_eager().decode('ascii')
            telnet_login.write(b"sys" + b"\n")

            time.sleep(3)
            print(xx)
            xx = telnet_login.read_very_eager().decode('ascii')
            time.sleep(3)
            print(xx)
            main()
            break
