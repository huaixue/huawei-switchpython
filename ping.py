import os
import threading
import socket

dizt = list()


disw = list()


def get_all_ip(gateway):
    # 通过一个单一的ip地址获取一个段的地址

    a = gateway.split('.')
    ip = list()
    for i in range(1, 255):
        ip.append(a[0] + '.' + a[1] + '.' + a[2] + '.' + str(i))
    return ip


def get_d_ip(ipd1, ipd2):
    # 通过输入的ip段获取地址
    a = ipd1.split('.')
    b = ipd2.split('.')
    ip = list()
    for i in range(int(a[3]), int(b[3]) + 1):
        ip.append(a[0] + '.' + a[1] + '.' + a[2] + '.' + str(i))
    return ip


def ping(ip):
    output = os.popen("ping -n 2 %s" % ip)
    result = output.read().encode('utf-8')
    if b'TTL' in result:
        a = ip + '...\t存活'
        dizt.append(a)

    else:
        b = ip + '...\t死亡'
        disw.append(b)


def scan_all_ip(ip):
    a = []
    aa = get_all_ip(ip)
    for ip in aa:
        t = threading.Thread(target=ping, args=(ip,))
        a.append(t)
    for i in a:
        i.start()
    for i in a:
        i.join()


def scan_d_ip(ipd1, ipd2):
    a = []
    aa = get_d_ip(ipd1, ipd2)
    for ip in aa:
        t = threading.Thread(target=ping, args=(ip,))
        a.append(t)
    for i in a:
        i.start()
    for i in a:
        i.join()


def ks_jc():
    while True:
        a = input("请输入需要检测的网段地址(192.168.1.x):")
        print("多线程启动中，请稍等")
        dizt.clear()
        disw.clear()
        scan_all_ip(a)
        ch = sorted(dizt, key=lambda x: (int(x.split('.')[3])))
        sw = sorted(disw, key=lambda x: (int(x.split('.')[3])))
        for i in ch:
            print(i)
        for h in sw:
            print(h)
        print("检测完成")
        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def ks_jc1(ip):  # 快速检测本机网段

        print("开始检测ing")
        print("多线程启动中，请稍等")
        dizt.clear()
        disw.clear()
        scan_all_ip(ip)
        ch = sorted(dizt, key=lambda x: (int(x.split('.')[3])))
        sw = sorted(disw, key=lambda x: (int(x.split('.')[3])))
        for i in ch:
            print(i)
        for h in sw:
            print(h)
        print("检测完成")



def zdj_jc():
    while True:
        a = input("请输入开始地址(192.168.1.x):")
        b = input("请输入结束地址(192.168.1.x):")
        print("多线程启动中，请稍等")
        dizt.clear()
        disw.clear()
        scan_d_ip(a, b)
        ch = sorted(dizt, key=lambda x: (int(x.split('.')[3])))
        sw = sorted(disw, key=lambda x: (int(x.split('.')[3])))
        for i in ch:
            print(i)
        for h in sw:
            print(h)
        print("检测完成")
        answer = input("是否返回上一级？y/n")
        if answer == 'y' or answer == 'Y':

            break
        else:
            continue


def mcd():
    print("-" * 30)
    print("1.快速存活检测")
    print("2.自定义地址段存活检测")
    print("3.检测本机IP网段")
    print("4.")
    print("5.")
    print("6.")
    print("7.")
    print("8.")
    print("9.")
    print("0.退出程序")
    print("-" * 30)


def main():
    while True:
        benji_data()
        mcd()
        try:
            xh = int(input("请选择："))
        except:
            print('请选择正确的选项')
        else:
            if xh in [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]:
                if xh == 0:
                    answer = input('您确定好退出系统吗？y/n')
                    if answer == 'y' or answer == 'Y':
                        print('谢谢您的使用！！！')
                        break
                    else:
                        continue
                elif xh == 1:
                    ks_jc()
                elif xh == 2:
                    zdj_jc()
                elif xh == 3:
                    ks_jc1(benji_data())
                elif xh == 4:
                    pass
                elif xh == 5:
                    pass
                elif xh == 6:
                    pass
                elif xh == 7:
                    pass
                elif xh == 8:
                    pass
                elif xh == 9:
                    pass


def benji_data():
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    print(f" 本机计算机名：{hostname}" + "\n", f"本机IP地址：{ip}")
    return ip


if __name__ == '__main__':


    main()





#print(sorted(dizt, key = lambda  x: (int(x.split('.')[3]))))

#print(dizt)   2